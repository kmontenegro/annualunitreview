## Sample Schedule (90-120 minute annual meeting)  
1- 5 minute introductions  
2- 5 minute review of what the team's mission/purpose  
3- 10 minute review of the previous year's top 2-3 points/areas of success  
4- 5-7 minute review of chief stumbling blocks/contratemps/issues in executing work  
5- 10 minute review of top lessons learned  
6- 20 minute presentation of the team's work plan  
7- 5-7 minutes highlighting new approaches to this work plan  
8- 5-7 minutes areas where additional support/resources might be needed for successful execution of the work plan  
9- 5 minute 24-month vision of how the work landscape might shift  
10- 20 minutes for questions/discussion  